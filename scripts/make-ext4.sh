#!/bin/bash

set -euo pipefail

image="$1"

time truncate -s 1T "$image"
time mke2fs -i 1024 -N 2000000000 -t ext4 -F "$image"
