#!/bin/bash

set -euo pipefail

# PER_LEVEL=100 # for 1 million files, for testing and timing
PER_LEVEL=1000 # for 1 billion files
export timestamp_file=$(pwd)/timestamp-reference
PARALLEL=${1:-5} # default to 5

export list=$(seq -w 1 $PER_LEVEL)

function populate_folder {
  local level1=$1
  # Dir_path=$2
  mkdir "loopback/$level1"
  cd "loopback/$level1"
  local level2
  for level2 in $list; do
    mkdir $level2
    cd $level2
    touch -r $timestamp_file $list
    cd ..
    touch -r $timestamp_file $level2
    echo $level1/$level2
  done # /level2
  cd ..
  touch -r $timestamp_file $level1
  # echo -n . # print a dot, for progress meter
  echo /$level1 # will use pv and line-mode
  # sleep 5
}

# main
date
export -f populate_folder
echo $list |\
  xargs --max-args 1 --max-procs $PARALLEL \
    bash -c 'populate_folder "$@"' _
echo
date
