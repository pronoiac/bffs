# using scripts to make many, many files
## why
Maybe you're wary of, or can't compile or run, custom binaries.
Maybe you're wondering "how much slower could it be?"

These were written for Debian on the Pi.


## preparation
I wrote them to do their work in `./loopback`.

* [mkfs-ext4.sh](mkfs-ext4.sh) - make a 1 TB sparse file, format it to ext4
* [mkfs-ext4-1g.sh](mkfs-ext4-1g.sh) -
  make a 1 GB sparse file, format it to ext4.
  suitable for holding a million empty files.
  used for the parallel multitouch prep.

## various attempts
> for more methods and statistics, see [my blog post on making these file systems](https://pronoiac.org/misc/2024/03/making-file-systems-with-a-billion-files/).

### workflow
My usual steps:
```
./make-ext4.sh $image
sudo mount $image loopback/
sudo chown $USER loopback/
# run a script, perhaps with pv to offer a progress meter and ETA
# for example:
time ./forest-parallel-multitouch-1bil.sh |\
  pv --line-mode --size 1000000 >\
  /dev/null
sudo umount loopback
```

### generator scripts
These are ordered, roughly, slowest to fastest.
These times were on an ext4 file system.
`forest-parallel-multitouch.sh` was run against faster storage: NAS.

| method                                        | clock time                    | files/second |
|-----------------------------------------------|-------------------------------|--------------|
| shell script: `forest-touch.sh`               | 31days (estimated, cancelled) | 375          |
| python script: `create_files.py`              | 1451m43.889s or 24hr11min43s  | 11480        |
| shell script: `forest-tar.sh`                 | 1299min16s or 21hr39min16s    | 12830        |
| shell script: `forest-multitouch.sh`          | 1157min54s or 19hr17min54sec  | 14390        |
| Rust program: `create-empty-files`            | 854min or 14hr14min           | 19500        |
| shell script: `forest-parallel-multitouch.sh` | 401min or 6hr41min            | 41500        |

* [`forest-touch.sh`](forest-touch.sh) - run `touch $file` in a loop, 1 billion times
* [`create_files.py`](create_files.py) - creates a file, 1 billion times.
from [Lars Wirzenius, take 1, repo](http://git.liw.fi/billion-files/).
* [`forest-tar.sh`](forest-tar.sh) - build a tar.gz with a million files, then unpack it, a thousand times. 
makes an effort for a consistent timestamp, for better compression.
* [`create-empty-files` lives in another repo](https://gitlab.com/larswirzenius/create-empty-files).
  included here for comparison.
* [`forest-multitouch.sh`](forest-multitouch.sh) - run `touch 0001 ... 1000` in a loop, 1 million times.
makes an effort for a consistent timestamp, for better compression.
* [`forest-parallel-multitouch.sh`](forest-parallel-multitouch.sh) -
  like multitouch, several at a time.
  will run with the number of processes given as an argument, defaults to 5.
  emits 1 million lines, for progress tracking.

### parallel multitouch preparation
> for more details, see [my blog post on parallel multitouch](pronoiac.org/misc/2024/06/file-systems-with-a-billion-files-making-forests-parallel-multitouch/)

To work out the best number of processes to run in parallel:
* the rule of thumb for `make -j` is, take the number of cores, and add one
* do smaller runs with a variable number of processes, and see what works best

Files:
* [`forest-parallel-multitouch-1mil.sh`](forest-parallel-multitouch-1mil.sh) -
  will run with the number of processes given as an argument, defaults to 5.
  emits 10k lines, one per 100 files, for use with `pv` to provide a progress meter.
  it doesn't log the output to a file; I scrolled through the output.
* [`benchmark-parallel-multitouch.sh`](benchmark-parallel-multitouch.sh) -
  infrastructure to automate calling the above, like
  making the drive image, mounting it, chown'ing it, calling `forest-parallel-multitouch-1mil.sh`, unmounting the image.
  adds a nice progress meter, using `pv`.
  makes 1 million files, with 1 to 10 processes.
  this took around 10 minutes to run.

## _so much_ archiving and compression
For more context, [the relevant blog post](pronoiac.org/misc/2024/09/file-systems-with-a-billion-files-archiving-and-compression/).
These use `pv`.

### generate multitouch ext4
[generate-multitouch-ext4.sh](generate-multitouch-ext4.sh) -
* generate a file system
* archive it to tar and gzip -1, which handles sparse files, fairly quickly
* checksum the image, the image from the tar, and the generated tar

### compress multitouch ext4
[compress-multitouch-ext4.sh](compress-multitouch-ext4.sh)
* run _all_ the compressors:
  * compress
  * decompress and checksum output
  * decompress to /dev/null for speed

#### features
* lots of `pv`, for a sense of how long the current step will take
* use [debug mode](https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_02_03.html) selectively, to show what it’s running, so I can later match and compare what I _meant__ to do with what I _actually_ did
* for each compressor: compress, then checksum, then benchmark decompression timing.
  this means, if a compressor is glitchy, it fails faster and halts the run.
  this is very useful and saves frustration when, say, `$compressor -1` crashes, and the `-9` takes days.
  (an earlier version: do all the compression, then benchmark all the decompression.)
* it’s much less error-prone than copypasta and careful editing.
  "is zstandard output .zst or .zstd?" was not an issue, after this
* set up to _not_ wipe out previous and partial runs.
  before that, I lost runs when I hit up arrow and Enter.

Confirming checksums is still a manual process.

This is a combination of a couple of versions I used.

Let me know if, for example, I didn't include arguments for multithread compression or decompression.
