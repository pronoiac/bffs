#!/bin/bash

set -euo pipefail

image="$1"

time truncate -s 1G "$image"
time mke2fs -i 1024 -N 2000000 -t ext4 \
  -E lazy_itable_init=0,lazy_journal_init=0 \
  -F "$image"
