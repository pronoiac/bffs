#!/bin/bash

set -euo pipefail

base=/mnt/billions/multitouch-ext4
image_filename=1tb-multitouch.img

### 1.1 multitouch, ext4
  # input=$base/../net/1tb-ext4-rust.img.tar.gzip-1
  output=$base/$image_filename
  image=$output
  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.skip" ]
  then
    # echo "skipping, $output exists"
    true
  else
    echo "### 1.1 multitouch, ext4"
    date
    set -x
    time ./make-ext4.sh $image
    time sudo mount $image loopback/
    time sudo chown $USER loopback/
    time ./scripts/forest-multitouch.sh | \
      pv --line-mode --size 1000 > \
      /dev/null
    time sudo umount loopback
    set +x
    date
    echo
  fi

### 1.2 multitouch, ext4, tar
  # input=$image
  output=$image.tar.gz-1
  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.skip" ]
  then
    # echo "skipping, $output exists"
    true
  else
    echo "### 1.2 multitouch, ext4, tar"
    date
    set -x
    size=$(du -h $image | cut -f 1)
    time tar cvSf - --directory $base $image_filename |\
      pv -N in-tar -c --size $size |\
      gzip -1 |\
      pv -N out-gzip-1 -c >\
      $output
    set +x
    date
    echo
  fi

### 1.3 multitouch, ext4, checksum image
  # input=$image
  output=$image.sha1
  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.partial" ]
  then
    true
  else
    echo "### 1.3 multitouch, ext4, checksum image"
    date
    set -x
    pv -N image -c $image |\
      sha1sum |\
      tee $output
    set +x
    date
    echo
  fi

### 1.4 multitouch, ext4, checksum image from tar.gz
  input=$image.tar.gz-1
  output=$input.unpack.sha1
  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.partial" ]
  then
    true
  else
    echo "### 1.4 multitouch, ext4, checksum image from tar.gz"
    date
    set -x
    time pv -N in-tar.gz-1 -c $input |\
      gzip -d |\
      pv -N ungzip -c |\
      tar xOvf - $image_filename |\
      sha1sum |\
      tee $output
    set +x
    cat $image.sha1
    date
    echo
  fi

### 1.5 multitouch, ext4, checksum tar from tar.gz
  input=$image.tar.gz-1
  output=$image.tar.gz-1-d.sha1
  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.partial" ]
  then
    true
  else
    echo "### 1.5 multitouch, ext4, checksum tar from tar.gz"
    date
    set -x
    time pv -N in-tar.gz-1 -c $input |\
      gzip -d |\
      pv -N tar -c |\
      sha1sum |\
      tee $output
    set +x
    date
    echo
  fi
