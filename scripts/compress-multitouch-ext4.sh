#!/bin/bash

set -euo pipefail

file_label="multitouch, ext4, tar" # e.g. "rust, ext4, tar"
in_file="1tb-multitouch.img.tar.gz-1"
base=/mnt/billions/multitouch-ext4
in_path=$base/$in_file
expected_size="256G" # du -h image
good_sha1_file=$base/$in_file-d.sha1

function recompress {
  # input=$tar_base.gz-1-d.gz-1
  output=$base/$in_file-d.$compressed_suffix
  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.skip" ]
  then
    true
    # echo "skipping, $output exists"
  else
    date
    echo "### $file_label, $compression_label, recompress"
    set -x
    time pv --name in-image.tar.gz-1 --cursor $in_path |\
      gzip -d |\
      pv --name image --cursor --size $expected_size |\
      $compression_command |\
      pv --name "out-$compressed_suffix" --cursor >\
      $output
    set +x
    date
    echo
  fi
}

function checksum {
  input=$in_path-d.$compressed_suffix
  output=$in_path-d.$compressed_suffix.sha1

  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.skip" ]
  then
    true
  else
    echo "### $file_label, $compression_label, decompress checksum"
    set -x
    time pv --name in --cursor $input |\
      $decompression_command |\
      pv --name out --cursor --size $expected_size |\
      sha1sum |\
      tee $output
    set +x
    echo "manual check:"
    cat $good_sha1_file
    date
    echo
  fi
}

function decompress_timing {
  input=$in_path-d.$compressed_suffix
  output=$in_path-d.$compressed_suffix.timing

  if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.skip" ]
  then
    true
    # echo "skipping, $output exists"
  else
    date
    echo "### $file_label, $compression_label, decompress timing"
    set -x
    time pv --name in-$compressed_suffix --cursor $input |\
      $decompression_command |\
      pv --name out-tar --cursor --size $expected_size > \
      /dev/null
    set +x
    touch $output
    date
    echo
  fi
}

# estimate data file size
# expected_size=$(du -h $base/$in_file | cut -f 1) # use image, not gz


# most interesting ones
  # zstd -1
    compression_label="zstd -1"
    compression_command="zstd -T0 -1"
    decompression_command="zstd -d"
    compressed_suffix="zstd-1"
    recompress; checksum; decompress_timing

  # plzip -0
    compression_label="plzip -0"
    compression_command="plzip --threads 4 -0"
    decompression_command="plzip -d"
    compressed_suffix="plzip-0"
    recompress; checksum; decompress_timing

  # zstd -9
    compression_label="zstd -19"
    compression_command="zstd -T0 -19 --long"
    decompression_command="zstd -d"
    compressed_suffix="zstd-19"
    recompress; checksum; decompress_timing

  # xz -1
    compression_label="xz -1"
    compression_command="xz -T0 -1"
    decompression_command="xz -T0 -d"
    compressed_suffix="xz-1"
    recompress; checksum; decompress_timing

  # xz -9
    compression_label="xz -9"
    compression_command="xz -T3 -M 4GiB -9"
    decompression_command="xz -T0 -d"
    compressed_suffix="xz-9"
    recompress; checksum; decompress_timing

  # gzip -1
    compression_label="gzip -1"
    compression_command="gzip -1"
    decompression_command="gzip -d"
    compressed_suffix="gz-1"
    recompress; checksum; decompress_timing

  # gzip -9
    compression_label="gzip -9"
    compression_command="gzip -9"
    decompression_command="gzip -d"
    compressed_suffix="gz-9"
    recompress ; checksum; decompress_timing


# less interesting
  # brotli -0
    compression_label="brotli -0"
    compression_command="brotli -0"
    decompression_command="brotli -d"
    compressed_suffix="brotli-0"
    recompress; checksum; decompress_timing

  # lz4 -1
    compression_label="lz4 -1"
    compression_command="lz4 -1"
    decompression_command="lz4 -d"
    compressed_suffix="lz4-1"
    recompress; checksum; decompress_timing

  # lz4 -9
    compression_label="lz4 -9"
    compression_command="lz4 -9"
    decompression_command="lz4 -d"
    compressed_suffix="lz4-9"
    recompress; checksum; decompress_timing

  # lrzip - 17hr?
    compression_label="lrzip"
    compression_command="lrzip --quiet"
    decompression_command="lrzip -d -p 4 --quiet" # -p 4 ?
    compressed_suffix="lrzip"
    recompress; checksum; decompress_timing

  # over 24 hours
  # zstd -19, skipping
    compression_label="zstd -19"
    compression_command="zstd -T0 -19 --long"
    decompression_command="zstd -d"
    compressed_suffix="zstd-19"
    recompress; checksum; decompress_timing


## bullseye only, not bookworm right now
  # lzop -1
    compression_label="lzop -1"
    compression_command="lzop -1"
    decompression_command="lzop -d"
    compressed_suffix="lzop-1"
    recompress; checksum; decompress_timing

  # pbzip2 -1
    compression_label="pbzip2 -1"
    compression_command="pbzip2 -1"
    decompression_command="pbzip2 -d"
    compressed_suffix="pbzip2-1"
    recompress; checksum; decompress_timing

  # pigz -1 - 1hr
    compression_label="pigz -1"
    compression_command="pigz -1"
    decompression_command="pigz -d"
    compressed_suffix="pigz-1"
    recompress; checksum; decompress_timing

  # pbzip2 -9 - 6hr
    compression_label="pbzip2 -9"
    compression_command="pbzip2 -9"
    decompression_command="pbzip2 -d"
    compressed_suffix="pbzip2-9"
    recompress; checksum; decompress_timing

  # pigz -9 - 7hr
    compression_label="pigz -9"
    compression_command="pigz -9"
    decompression_command="pigz -d"
    compressed_suffix="pigz-9"
    recompress; checksum; decompress_timing

  # below: over 1 day
  # lzop -9
    compression_label="lzop -9"
    compression_command="lzop -9"
    decompression_command="lzop -d"
    compressed_suffix="lzop-9"
    recompress; checksum; decompress_timing

  # plzip -9 - though -0 seemed ok on bookworm?
    compression_label="plzip -9"
    compression_command="plzip --threads 4 -9"
    decompression_command="plzip -d"
    compressed_suffix="plzip-9"
    recompress; checksum; decompress_timing
