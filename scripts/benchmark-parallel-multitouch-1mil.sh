#!/bin/bash

set -euo pipefail

base=$(pwd)/parallel-multitouch # /mnt/billions/parallel-multitouch
mkdir -p $base
max_processes=10

echo "### parallel multitouch, 1 to 10 process"
  for parallel in $(seq 1 $max_processes); do
    # input=$base/../net/1tb-ext4-rust.img.tar.gzip-1
    output=$base/1gb-parallel-multitouch-x$parallel.img
    image=$output
    if [ -e "$output" ] || [ -e "$output.partial" ] || [ -e "$output.skip" ]
    then
      echo "skipping, $output exists"
    else
      date
      set -x
      ./make-ext4-1g.sh $image 
      sudo mount $image loopback/
      sudo chown $USER loopback/
      time ./forest-parallel-multitouch-1mil.sh $parallel |\
        pv --line-mode --size 10000 > \
        /dev/null
      sudo umount loopback
      set +x
    fi
    date
    echo
  done # /parallel

