#!/usr/bin/env Rscript

# so this is sorta a worklog...

# require(ggplot2)
library(ggplot2)

data1 <- read.csv("compressors-rust-ext4.csv", header=TRUE, stringsAsFactors=FALSE)

# skip this for screen output
png("rust-decompression-time-vs-compressed-file-size.png", width = 800, height = 800, res = 120)
# dev.off()

p <- ggplot(data1, 
  aes(
    x = decompression.seconds / 60,
    compressed.size.in.bytes / (1024 * 1024 * 1024))
  ) +
  geom_point(aes(shape = OS, color = OS)) +
  geom_text(aes(label = compressor), nudge_y = 0.5) # +
  # scale_x_log10()

orig_volume <- 270 # est; fill in

# vertical scale: space
p + scale_y_continuous (
  # left / first axis
  name = "compressed space, in GiB",

  # right / second axis
  sec.axis = sec_axis(~ orig_volume / ., name = "compression ratio")
) +
ggtitle("Decompression time vs compressed file size, Rust-generated file system",
  subtitle = 'Better performance to the bottom left') +

# horizontal scale: time
# scale_x_continuous (
scale_x_log10 (
  # first axis, at the bottom
  name = "decompression time, in minutes, log scale",

  # second axis, at the top
  # x scale: CSV is in seconds, bottom axis is in min
  sec.axis = sec_axis (
    ~ (orig_volume * 1000) / (60 * .),
    name = "decompression speed, MiB / sec"
  )
) +
theme(legend.position="bottom")


dev.off() # for png output

