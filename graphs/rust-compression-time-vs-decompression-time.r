#!/usr/bin/env Rscript

# so this is sorta a worklog...

# require(ggplot2)
library(ggplot2)
library(scales)
library(ggrepel)


data1 <- read.csv("compressors-rust-ext4.csv", header=TRUE, stringsAsFactors=FALSE)

# skip this for screen output
png("rust-compression-time-vs-decompression-time.png", width = 800, height = 800, res = 120)
# dev.off()

p <- ggplot(data1, aes(
    x = compression.minutes / 60,
    y = decompression.seconds / 60)
  ) +
  geom_point(aes(shape = OS, color = OS)) +
  geom_text(aes(label = compressor), nudge_y = 0.03) +
  scale_x_log10()

orig_volume <- 270 # est; fill in

# vertical scale: compression time
p + scale_x_log10 (
  # bottom / first axis
  name = "compression time, in hours, log scale",

  # top / second axis
  # x scale: CSV is in minutes, bottom axis is in hours
  sec.axis = sec_axis (
    ~ (orig_volume * 1000) / (3600 * .),
    name = "compression speed, MiB / sec",
    labels = label_comma(drop0trailing = TRUE)
  )
) +
ggtitle("Decompression time vs compression time, Rust-generated file system",
  subtitle = 'Better performance to the bottom left') +

# horizontal scale: decompression time
# scale_y_continuous (
scale_y_log10 (
  # first axis, at the left
  name = "decompression time, in minutes, log scale",

  # second axis, at the right
  # csv: seconds, y: minutes
  sec.axis = sec_axis (
    ~ (orig_volume * 1000) / (60 * .),
    name = "decompression speed, MiB / sec"
  )
) +
theme(legend.position="bottom")


dev.off() # for png output
