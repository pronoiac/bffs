#!/usr/bin/env Rscript

library(ggplot2)
library(scales)
# library(ggrepel)

data1 <- read.csv("generators-ext4.csv", header=TRUE, stringsAsFactors=FALSE)

# skip this for screen output
png("generators-compression-time-vs-file-size.png", width = 800, height = 800, res = 120)
# dev.off()

p <- ggplot(data1,
  aes(
    x = compression_minutes / 60.0,
    y = tar_compressed_size_bytes / (1024 * 1024 * 1024))
  ) +
  geom_point(aes(shape = generator, color = generator)) +
  # guides(shape = "none", color = "none") +
  geom_text(
    aes(label = compressor),
    nudge_y = 0.5
  ) +
  scale_x_log10()

# orig_volume <- 270 # est; fill in
# these vary by generator

# vertical scale: space
p + scale_y_continuous (
  # left / first axis
  name = "compressed file size, in GiB",

  # skipping "compression ratio" as the starting tar files are different sizes
  # right / second axis
  # sec.axis = sec_axis(~ orig_volume / ., name = "compression ratio")
) +
ggtitle("Compression time vs compressed file size, for multiple generators",
  subtitle = 'Better performance to the bottom left') +

  # horizontal scale: time
  scale_x_log10(
    # first axis, at the bottom
    name = "compression time, in hours, log scale",

    # skipping second axis, as the starting tar files are different sizes
    # second axis, at the top
    # x scale: CSV is in minutes, bottom axis is in hours
    # sec.axis = sec_axis (
    #   ~ (orig_volume * 1000) / (3600 * .),
    #   name = "compression speed, MiB / sec",
    #   labels = label_comma(drop0trailing = TRUE)
    # ) # ,
    expand = expansion(mult = 0.1) # avoid cropping labels
  ) +
  theme(legend.position = "bottom")

dev.off() # for png output
