# graphs

## About
I made these graphs with R, after bouncing off of Google Docs and LibreOffice.
For more context, [see my blog post about archiving and compression](pronoiac.org/misc/2024/08/file-systems-with-a-billion-files-archiving-and-compression/).

## Rust drive image
* data - [csv document](compressors-rust-ext4.csv)
  * edit: lrzip decompression was left out.
    it took 22 hours.
    even on a log scale, it was the only data point in the top half of the graph.
* compression time vs compressed volume -
  [R code](rust-compression-time-vs-compressed-file-size.r)
  ![rust compression time vs compressed file size graph](rust-compression-time-vs-compressed-file-size.png)
* compression time vs decompression time -
  [R code](rust-compression-time-vs-decompression-time.r)
  ![compression time vs decompression time graph](rust-compression-time-vs-decompression-time.png)
* compressed volume vs decompression time -
  [R code](rust-decompression-time-vs-compressed-file-size.r)
  ![compressed volume vs decompression time graph](rust-decompression-time-vs-compressed-file-size.png)

## how well do file systems from different generators compress?
* data - [csv document](generators-ext4.csv)
* [R code]()
* graph:
  ![compression time vs compressed file size graph](generators-compression-time-vs-file-size.png)

## R dependencies
I ran the R interpreter - `r` - and something like:
```
install.packages("ggrepel")
```

I don't remember having to do that for `ggplot2` or `scales`, but I might have forgotten.
