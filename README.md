# billion file file systems

## about
> [I'll excerpt my own blog post.](https://pronoiac.org/misc/2024/03/file-systems-with-a-billion-files-intro-toc/)

[Lars Wirzenius blogged about making a file system with a billion empty files](https://blog.liw.fi/posts/2024/billion/).
Working on that scale can make ordinarily quick things very slow - like taking minutes to list folder contents, or delete files.
Initially, I was curious about how well general-purpose compression like `gzip` would fare with the edge case of gigabytes of zeroes, and then I fell down a rabbit hole.
I found a couple of major speedups, tried a couple of other formats, and tried some other methods for making _so many_ files.

## what's in here
* the [scripts](scripts/) directory: preparing and generating these file systems
* [graphs](graphs/): graphs and the code to generate them, covering compression

## resources
* [my blog posts - intro and Table of Contents](https://pronoiac.org/misc/2024/03/file-systems-with-a-billion-files-intro-toc/)

From Lars Wirzenius:
* [first blog post (2020)](https://blog.liw.fi/posts/2020/10/01/a_billion_files/)
* [a second blog post (2024)](https://blog.liw.fi/posts/2024/billion/), which was my entry point
